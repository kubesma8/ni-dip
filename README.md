# RSA - útok na elektromagnetickém spektru

Soubor traces.bin obsahuje řádově 100-150 tisíc traců, délka je uložena vždy v přiloženém souboru traceLength.txt. OT je v plaintext.txt, CT je v ciphertext.txt. Formát je 32-bit floating point v pořadí little-endian.

[OneDrive složka s naměřenými daty.](https://campuscvut-my.sharepoint.com/:f:/g/personal/kubesma8_cvut_cz/EtaZByvEHD9Lq0X_K6zeO6cBdKQ8FCN_Ma2s_xUTijikPw?e=9sww8A)
