# Smartcard power side channel measurement script
# Authors: Marina Shchavleva, Filip Kodytek, Jiri Bucek

# pip install pyvisa pyvisa-py pyusb
# pip install pyscard
import random
from time import sleep
import card
import oscilloscope
from rsa_api_full_example import *

def visualizeIQ(time, IQ):
    fig = plt.figure(1, figsize=(15, 10))
    fig.suptitle('I and Q vs Time', fontsize='20')
    ax1 = plt.subplot(211, facecolor='k')
    ax1.plot(time * 1000, np.real(IQ), color='y')
    ax1.set_ylabel('I (V)')
    ax1.set_xlim([time[0] * 1e3, time[-1] * 1e3])
    ax2 = plt.subplot(212, facecolor='k')
    ax2.plot(time * 1000, np.imag(IQ), color='c')
    ax2.set_ylabel('Q (V)')
    ax2.set_xlabel('Time (msec)')
    ax2.set_xlim([time[0] * 1e3, time[-1] * 1e3])
    plt.tight_layout()
    plt.show()
    sleep(0.05)
    plt.close()
    return fig

def visualizeAM(time, AM):
    fig = plt.figure(1, figsize=(15, 10))
    fig.suptitle('Amplitude of BW in Time', fontsize='20')
    ax1 = plt.subplot(211, facecolor='k')
    ax1.plot(time * 1000, np.real(AM), color='y')
    ax1.set_ylabel('A (V)')
    ax1.set_xlabel('Time (msec)')
    ax1.set_xlim([time[0] * 1e3, time[-1] * 1e3])
    plt.tight_layout()
    plt.show()
    sleep(0.05)
    plt.close()
    return fig

def list_resources(resources: list, resource_name: str):
    if not resources:
        print('no', resource_name, 'available')
    else:
        print('available', resource_name + ':')
        for resource_id in range(len(resources)):
            print('[', resource_id, '] ', resources[resource_id])

def list_scopes():
    found_oscilloscopes = oscilloscope.get_oscilloscopes()
    list_resources(found_oscilloscopes, 'oscilloscopes')


def list_readers():
    found_readers = card.get_readers()
    list_resources(found_readers, 'readers')

# def channel_meas(scope, n):
#     scope.command_check(":WAVeform:SOURce", 'CHANnel{}'.format(n))
#     trace = scope.query_binary(':WAVeform:DATA?')
#     return trace

def printscreen(scope: oscilloscope):
    with open ('printscreen.png', "wb") as fscreen:
        # /* Download the screen image */
        scope.write(":HARDcopy:INKSaver OFF")
        # /* Read screen image. */
        screen_data = scope.query_binary(":DISPlay:DATA? PNG, COLor")
        fscreen.write(screen_data)

# Infinite test run. Can be interrupted by pressing CTRL-C
def test_run(my_card: card):
    print ("Infinite run, press CTRL-C to break.")
    try:
        while True:
            my_card.send_encrypt([i for i in range(16)])
            sleep(0.01)
    except KeyboardInterrupt:
        pass
    

def measure_random(my_card: card, num_of_measurements):
    with open('traceLength.txt', "w") as finfo, open ('traces.bin', "wb") as fdata,\
        open('plaintext.txt',"w") as fplaintext, open('ciphertext.txt',"w") as fciphertext:
        
        # NASTAVENI ZARIZENI
        search_connect()
        config_trigger()
        cf = 1e7
        refLevel = 30
        iqBw = 4e7
        recordLength = 154e3

        #Radio Dechovka, Praha 2023, AM
        #cf = 126e1
        #iqBw = 1e4

        time = config_block_iq(cf, refLevel, iqBw, recordLength)
        print("Timestamp number:",len(time))

        # NASTAVENI HRUBOSTI MERENI
        # Requested points per trace - change if necessary. 
        # If less points are required, you can set the limit here.
        # If more points are required, disable channel 1 on the scope (trigger will still work)
        # If you need yet more points, change the overall settings to use the SRAT command.
        
        print("Warmup cycle")
        #dummy card op - wake up card, warm up
        for i in range(100):
            my_card.send_encrypt([i for i in range(16)])

        print("Measurement cycle")
        # Measurement cycle
        for i in range(num_of_measurements):
            card_pt = list(random.randbytes(16))
            print(f"Trace {i}")

            recordLength = int(recordLength)
            ready = c_bool(False)
            iqArray = c_float * recordLength
            iData = iqArray()
            qData = iqArray()
            outLength = 0
            rsa.DEVICE_Run()

            # CEKANI NA TRIGGER
            rsa.IQBLK_AcquireIQData()
            sleep(0.05) # Wait for the scope to arm its trigger

            card_ct = my_card.send_encrypt(card_pt)
            sleep(0.05)

            # ZISKAME DATA ZE SPEKTROMETRU
            while not ready.value:
                rsa.IQBLK_WaitForIQDataReady(c_int(100), byref(ready))
                rsa.IQBLK_GetIQDataDeinterleaved(byref(iData), byref(qData),
                                     byref(c_int(outLength)), c_int(recordLength))

            if i == 0:
                #tracelength = scope.query(':WAVeform:POINts?')
                print(recordLength, file = finfo)
            
            rsa.DEVICE_Stop()

            # ZAPISEME BINARNI DATA ZE SPEKTROMETRU
            IQ = np.array(iData) + 1j * np.array(qData)
            AM = np.array(np.sqrt(np.square(np.array(iData)) + np.square(np.array(qData))))
            #print(AM)
            
            #fdata.write(AM[0])
            print(AM)
            #fdata.write(AM)
            AM.tofile(fdata)
            #fdata.write(AM[0])

            #print(iData[0])
            #print(IQ[0])

            # Chceme videt IQ nebo cisty signal, resp AM?
            #fig = visualizeIQ(time,IQ)
            #fig = visualizeAM(time,AM)
            
            print(" ".join(map(lambda b: "%02x" % b, card_pt)), file = fplaintext)
            print(" ".join(map(lambda b: "%02x" % b, card_ct)), file = fciphertext)


# =============================== MAIN ===================================
if __name__ == '__main__':
    # list_scopes()
    list_readers()
    print('----------------')
    # scope = oscilloscope.Oscilloscope()
    reader = card.Reader()
    print(reader.send_encrypt([i for i in range(16)]))

    print('------- DUMMY TEST, SET UP YOUR SCOPE ---------')
    # test_run(reader)

    # scope.save_conf('scope_setup.conf')
    # printscreen(scope)

    # scope.load_conf('scope_setup.conf')

    # print('------- MEASUREMENT WITH CARD AND SCOPE ---------')
    measure_random(reader, 500)


